const { Router } = require('express');
const { check } = require('express-validator');
const { validar } =  require('../middlewares/validar_campos');
const { create, getAll, desactivar, deNuevo, getAllExit } = require('../controllers/postulacionController');


const router = Router();

router.post('/postulacion/create', [
    check('id_proyecto', 'El id_proyecto es obligatorio').not().isEmpty().isNumeric(),
    check('id_user', 'El id_user es obligatorio').not().isEmpty().isNumeric(),
    validar
],
create);
router.post('/postulacion/:id', getAll);
router.post('/postulacion/getAllExit/:id', getAllExit);
router.put('/postulacion/desactivar', [
    check('id_proyecto', 'El id_proyecto es obligatorio').not().isEmpty().isNumeric(),
    check('id_user', 'El id_user es obligatorio').not().isEmpty().isNumeric(),
    validar
],
desactivar);
router.put('/postulacion/deNuevo', [
    check('id_proyecto', 'El id_proyecto es obligatorio').not().isEmpty().isNumeric(),
    check('id_user', 'El id_user es obligatorio').not().isEmpty().isNumeric(),
    validar
],
deNuevo);

module.exports = router;