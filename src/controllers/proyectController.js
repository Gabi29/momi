const { pool } = require("../database/config");

//Obtener todos los proyectos
const getAll = async (req,res) => {
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM proyectos',(err, rows) => {
            if (err) reject(err);
            resolve(rows);
            console.log(rows);
            res.json({
                ok: true,
                text: 'Todos los proyectos',
                data: rows
            });
        });
    });
}

//Obtener solo uno
const getOne = async (req,res) => {
    const { id } = req.params;
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM proyectos WHERE id = ?',[id],
        (err, rows) => {
            if (err) reject(err.message);
                if(rows.length>0){
                res.json({
                    ok: true,
                    text: 'Obtiene solo uno proyecto con el id: '+ id,
                    data: rows[0]
                });
            }else{
                res.json({
                    ok: false,
                    text:'Proyecto no encontrado'
                });
                }
        });
    });
}

//Crear un proyecto
const create = async (req, res) => {
    return new Promise((resolve, reject) => {
        pool.query('INSERT INTO proyectos set ?', [req.body]);
        console.log(req.body);
        res.json({
            ok: true,
            text: 'Proyecto ' + req.body.nombre + ' creado'
        });
    });
}

//Borrar un proyecto
const eliminar = async (req, res) => {
    const { id } =  req.params;
    return new Promise((resolve, reject) => {
        pool.query('DELETE FROM proyectos WHERE id = ?',[id],
            (err, rows) => {
                if (err) reject(err.message);
                res.json({
                    ok: true,
                    text:'Proyecto eliminado'});
        });
    });
}

//Actualizar un proyecto
const update = async (req, res) => {
    const { id } =  req.params;
    return new Promise((resolve, reject) => {
        pool.query('UPDATE proyectos set ? WHERE id = ?',[req.body,id],
            (err, rows) => {
                if (err) reject(err.message);
                res.json({
                    ok: true,
                    text:'Proyecto actualizado'
                });
        });
    });
}

//Activar un proyecto
const activar = async (req, res) => {
    const { id } =  req.params;
    return new Promise((resolve, reject) => {
        pool.query('UPDATE proyectos set activo = 1 WHERE id = ?',[id],
            (err, rows) => {
                if (err) reject(err.message);
                res.json({
                    ok: true,
                    text:'El proyecto ha sido activado'
                });
        });
    });
}

//Desactivar un proyecto
const desactivar = async (req, res) => {
    const { id } =  req.params;
    return new Promise((resolve, reject) => {
        pool.query('UPDATE proyectos set activo = 0 WHERE id = ?',[id],
            (err, rows) => {
                if (err) reject(err.message);
                res.json({
                    ok: true,
                    text:'El proyecto ha sido desactivado'
                });
        });
    });
}

module.exports = {
    getAll,
    getOne,
    create,
    eliminar,
    update,
    activar,
    desactivar
}